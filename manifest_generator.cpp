//
// Created by Keith Webb on 10/15/16.
//


#include "md5.hpp"

#include <cstdlib>
#include <string>
#include <iostream>
#include <fstream>

#include <stack>
#include <vector>

#include <boost/filesystem.hpp>

#include "compression.hpp"

int main(int argc, char* argv[])
{
    if(argc < 3)
    {
        /* Parameters
         * Arg 0 = Full App Path and EXE
         * Arg 1 = Full Path to Directory to Generate Manifest For
         * Arg 2 = Output File Name
         */
        return 1;
    }

    std::string target_path_s = argv[1];
    std::string target_file = argv[2];

    boost::filesystem::path target_path(target_path_s);

    std::stack<boost::filesystem::path> paths;
    std::vector<boost::filesystem::path> files;

    paths.push(target_path);

    while(paths.size() != 0)
    {
        auto p = paths.top();
        paths.pop();

        for (boost::filesystem::directory_entry& x : boost::filesystem::directory_iterator(p))
        {
            if(boost::filesystem::is_directory(x.path()))
            {
                // Avoid Hidden Directories
                if(x.path().filename().string()[0] != '.')
                {
                    paths.push(x.path());
                }
            }
            if(boost::filesystem::is_regular_file(x.path()))
            {
                // Avoid Adding Manifest to Itself Across Multiple Runs
                if(x.path().filename() == target_file || x.path() == target_file)
                {
                    continue;
                }

                // Avoid Hidden Files
                if(x.path().filename().string()[0] == '.')
                {
                    continue;
                }

                files.push_back(x.path());
            }
        }
    }

    MD5 md5;
    std::string file_output;
    for(auto& p : files)
    {
        std::string s = p.lexically_relative(target_path).string();

        std::string fp = p.string();
        std::string m = md5.digestFile(fp.c_str());

        file_output += s + ":" + m + "\n";
    }

    std::ofstream file(target_file, std::ios::trunc);
    file << compress_string(file_output);
//    file << file_output;
    file.close();

    return 0;
}
