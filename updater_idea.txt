Auto Updater Idea : Keith Webb

The idea is for the application to do all of the updating work,
leaving our server just as a file host. This saves the server
from having to determine complex solutions to update the user.

While Jacob has created an Auto Updater for MoCap Studio, and
therefore the PrioVR Utility, it has several drawbacks that I
can improve on with this app. To get a better idea of why this
app is better, lets list some problems with Jacob's solution.

Bad Things About Jacob's Solution
  - If one file is updated across multiple updates, then it will
    be downloaded multiple times, wasting network and time
  - Creating an update for your app is a developer nightmare,
    as each update file must be very detailed in how the app
    has changed from the previous version
  - If a file is removed, the update process does not delete it
  - Deploying an update requires re-uploading files many times
    over, which is a waste of disk space on the server
  - You cannot update Electron as the updater is running inside
    of it, which keeps the app set a one Electron version

Good Things About Jacob's Solution
  - The user can update without downloading the whole installer
  - The app can update without relying on server logic
  - The app can figure out what files to download and update

My solution will keep all of the good things about Jacob's
solution while also fixing all of the bad things too.

The Updater consists of two executables, one for the
users (AutoUpdater) and one for developers (ManifestGenerator).

Auto Updater will get the latest version string from the website
and compare it to the current version string. If a newer one exists
on the website, then it will download a manifest file and uncompress it.
This manifest file is zlib compressed and contains a listing of each
and every file in the project, relative to the application directory,
and the MD5 hash of that file. The app will then perform a MD5 scan of
the app root directory and create its own listing. A comparison will
then be performed. Files that exist in the app listing but not the web
listing will be deleted. Files that exist in the web listing but not in
the app listing will be downloaded from the server, and added to the app.
Files that exist in both will have their MD5 checksum compared.
If there is a difference, then the updater will download the latest
version from the server and replace the existing file.

The developer side of the update process is simplified as well.
To create an update, first run the Manifest Generator on the directory
containing the application. The generator takes an absolute path to the
app root directory and a name for the output file. The exact directory of
the app that the manifest ran on will then be uploaded to the file server.
The manifest file will also be uploaded. And that is it!

This solution keeps the user bandwidth low by avoiding the whole installer.
It also means that the server is just a file server and has no job in the
update process. The updater can still determine what files need to be
downloaded and perform the update while fixing the problems described above.

This solution fixes many of Jacob's problems. First, a file that needs updating
and that has changed multiple times across versions will only be downloaded once.
Second, the process of creating an update is extremely easy for the developer.
Third, the updater will remove files and folders that are no longer needed. Certain
exceptions will be required to keep the app un-installer, and other misc files, from
being deleted by the updater. Fourth, only one version of the un-extracted files
will be on the server at once, saving disk space on the server. Fifth, since the
updater has no ties to Electron, it can perform updates to Electron as well.

This updater is more customizable, efficient, and powerful than the existing process.
Conversion to this method will require minimal code rework and can even be updated to
with the existing updater method if need be. The updater itself can be updated as well,
with a simple file copier helper program. The updater can tell when it needs updating
but since it is running, it will download the new updater under a different name, and
then spawn the helper and kill itself. The helper will replace the existing updater,
delete the temporary one, and then restart the main application. The helper is of course
customizable, and can be updated itself through the main updater.

And now, on to the code!