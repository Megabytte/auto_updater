//
// Created by Keith Webb on 10/16/16.
//

#pragma once

#include <string>
#include <fstream>
#include <map>

class ConfigReader
{
public:
    ConfigReader(std::string file_name="", char break_char='=');
    ~ConfigReader();

    void load(std::string file_name);

    void decode(std::string file_contents);

    void set_default_key_value(std::string key, std::string value);

    std::string get_value(std::string key);

    void set_break_character(char break_char) { break_character = break_char; }

    std::map<std::string, std::string>& get_map() { return mMap; };
private:
    char break_character;
    std::map<std::string, std::string> mMap;
    std::ifstream mFileHandle;
};


