//
// Created by Keith Webb on 10/16/16.
//

#include <sstream>
#include "config_reader.hpp"

ConfigReader::ConfigReader(std::string file_name, char break_char) : break_character(break_char)
{
    if(file_name != "")
    {
        load(file_name);
    }
}

ConfigReader::~ConfigReader()
{
    if(mFileHandle.is_open())
    {
        mFileHandle.close();
    }
}

void ConfigReader::load(std::string file_name)
{
    mFileHandle.open(file_name);

    if(!mFileHandle.is_open())
    {
        throw std::runtime_error(file_name + " could not be loaded!");
    }

    std::string line;
    while (getline (mFileHandle, line))
    {
        std::string key, value;
        bool has_hit_divider = false;
        for(unsigned long i = 0; i < line.size(); i++)
        {
            char character = line[i];

            if(character == break_character && !has_hit_divider)
            {
                has_hit_divider = true;
                continue;
            }

            if(has_hit_divider)
            {
                value += character;
            }
            else
            {
                key += character;
            }
        }

        mMap[key] = value;
    }
}

void ConfigReader::set_default_key_value(std::string key, std::string value)
{
    auto found = mMap.find(key);
    if(found == mMap.end())
    {
        mMap[key] = value;
    }
}

std::string ConfigReader::get_value(std::string key)
{
    auto found = mMap.find(key);
    if(found != mMap.end())
    {
        return found->second;
    }

    throw std::runtime_error(key + " not found!");
}

void ConfigReader::decode(std::string file_contents)
{
    std::string line;

    std::istringstream ss(file_contents);

    while (getline (ss, line))
    {
        std::string key, value;
        bool has_hit_divider = false;
        for(unsigned long i = 0; i < line.size(); i++)
        {
            char character = line[i];

            if(character == break_character && !has_hit_divider)
            {
                has_hit_divider = true;
                continue;
            }

            if(has_hit_divider)
            {
                value += character;
            }
            else
            {
                key += character;
            }
        }

        mMap[key] = value;
    }
}
