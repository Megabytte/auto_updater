//
// Created by Keith Webb on 10/15/16.
//


#include "md5.hpp"

#include <cstdlib>
#include <string>
#include <iostream>
#include <fstream>

#include <stack>
#include <vector>
#include <map>

#include <boost/filesystem.hpp>
#include <curl/curl.h>

#include "compression.hpp"
#include "network.hpp"

#include "config_reader.hpp"

std::string site_location;
std::string app_location;
std::string updater_exe_name;

#if defined(_WIN32)
std::string separator = "\\";
#else
std::string separator = "/";
#endif

// TODO: Give Updater Ability to Update Itself and Its Helper

std::map<std::string, std::string> analyze_maps(std::map<std::string, std::string> app, std::map<std::string, std::string> web)
{
    std::map<std::string, std::string> results;
    for(auto& web_pair : web)
    {
        std::string file = web_pair.first;
        std::string hash = web_pair.second;

        auto found = app.find(file);
        if(found != app.end())
        {
            // In Both Manifests
            if(hash != found->second)
            {
                // Need Update
                results[file] = "Update";
            }

            app.erase(found);
        }
        else
        {
            // Not Found in App, Needs Added
            results[file] = "Add";
        }
    }

    // Only Files That Need Deleting Are Left
    for(auto& app_pair : app)
    {
        results[app_pair.first] = "Delete";
    }

    return results;
}

void handle_actions(std::map<std::string, std::string> file_actions)
{
    for(auto& file_actions_pair : file_actions)
    {
        std::string file = file_actions_pair.first;
        std::string action = file_actions_pair.second;

        // TODO: Give Updater Ability to Update Itself and Its Helper
        if(file == updater_exe_name)
        {

        }

        if(action == "Update")
        {
            std::cout << file << " needs Updating." << std::endl;

            // Unneeded But Paranoid Check to Make Sure Directory Exists
            {
                boost::filesystem::path path(app_location + separator + file);
                path = path.parent_path();
                if(!boost::filesystem::exists(path))
                {
                    boost::filesystem::create_directories(path);
                    std::cout << "Created: " << path.string() << "\n";
                }
            }

            std::ofstream oss(app_location + separator + file, std::ios::out | std::ios::trunc | std::ios::binary);
            if(CURLE_OK == curl_read(site_location + "/" + file, oss))
            {
                std::cout << file << " updated.\n";
            }
            else
            {
                std::cout << "Update of " << file << " failed.\n";
            }
            oss.close();
        }
        else if(action == "Add")
        {
            std::cout << file << " needs Adding." << std::endl;

            boost::filesystem::path path(app_location + separator + file);
            path = path.parent_path();

            if(!boost::filesystem::exists(path))
            {
                boost::filesystem::create_directories(path);
                std::cout << "Created: " << path.string() << "\n";
            }

            std::ofstream oss(app_location + separator + file, std::ios::out | std::ios::trunc | std::ios::binary);
            if(CURLE_OK == curl_read(site_location + "/" + file, oss))
            {
                std::cout << file << " added.\n";
            }
            else
            {
                std::cout << "Add of " << file << " failed.\n";
            }
            oss.close();
        }
        else if(action == "Delete")
        {
            std::cout << file << " needs Deleting." << std::endl;
            std::string temp = app_location + separator + file;
            if(std::remove(temp.c_str()) == 0)
            {
                std::cout << file << " deleted.\n";
            }
            else
            {
                std::cout << "Deleting " << file << " failed.\n";
            }

            boost::filesystem::path path(app_location + separator + file);
            path = path.parent_path();

            if(boost::filesystem::is_empty(path))
            {
                boost::filesystem::remove(path);
                std::cout << "Deleted: " << path.string() << "\n";
            }
        }
        else
        {
            std::cout << "Unknown Action: " << action << " for file: " << file << "." << std::endl;
        }
    }
}

int main(int argc, char* argv[])
{
    if(argc < 2)
    {
        return 1;
    }

    boost::filesystem::path exe_path(argv[0]);

    std::cout << updater_exe_name << "\n";

    ConfigReader settings_reader("AutoUpdater.config");

    site_location = settings_reader.get_value("SITE_FOLDER");

    curl_global_init(CURL_GLOBAL_DEFAULT);

    std::string target_path_s = argv[1];

    app_location = target_path_s;

    boost::filesystem::path target_path(target_path_s);

    std::stack<boost::filesystem::path> paths;
    std::vector<boost::filesystem::path> files;

    paths.push(target_path);

    while(paths.size() != 0)
    {
        auto p = paths.top();
        paths.pop();

        for(boost::filesystem::directory_entry& x : boost::filesystem::directory_iterator(p))
        {
            if(boost::filesystem::is_directory(x.path()))
            {
                // Avoid Hidden Directories
                if(x.path().filename().string()[0] != '.')
                {
                    paths.push(x.path());
                }
            }
            if(boost::filesystem::is_regular_file(x.path()))
            {
                // Avoid Hidden Files
                if(x.path().filename().string()[0] == '.')
                {
                    continue;
                }

                files.push_back(x.path());
            }
        }
    }

    std::map<std::string, std::string> file_to_md5_map;

    MD5 md5;
    for(auto& p : files)
    {
        std::string s = p.lexically_relative(target_path).string();
        std::string fp = p.string();
        std::string m = md5.digestFile(fp.c_str());
        file_to_md5_map[s] = m;
    }

    std::string src;

    src += settings_reader.get_value("SITE_FOLDER");
    src += settings_reader.get_value("FILE");

    ConfigReader manifest_reader;
    manifest_reader.set_break_character(':');

    std::ostringstream oss;
    if(CURLE_OK == curl_read(src, oss))
    {
        // Web page successfully written to string
        std::string manifest = oss.str();

        manifest = decompress_string(manifest);

        manifest_reader.decode(manifest);

        std::map<std::string, std::string> results = analyze_maps(file_to_md5_map, manifest_reader.get_map());

        if(results.size() == 0)
        {
            std::cout << "No Updater Action Required\n";
        }
        else
        {
            handle_actions(results);
        }
    }

    curl_global_cleanup();
    return 0;
}
