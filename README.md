# Updater README #

This project's goal is to implement a Project Updater solution that can be customized to a particular project's needs.

It depends on three libraries at the moment, Boost Filesystem, libCURL, and zlib.

The project builds three different executables, Auto Updater, Manifest Generator, and Simple Helper. The Auto Updater and Simple Helper are for the user's computer and the Manifest Generator is for the application developer.

## How To Use ##
To use this updater, you must have your web server configured and have the files uploaded. Make sure that your web server doesn't modify file when served, like html files, as this will mess up the manifest and make it seem that the file will always need updating. You will run manifest generator on the directory you want to be updated, and it will output a manifest file. You will upload the entire directory of your app to your server and the manifest file. You will then configure the config files for App Updater and that should be it! Be warned that this is under development and there is no guarantee that this will work for you.

## How It Works ##
To be written.

Further documentation on this software and how it works is to come.

This project is free for anyone who wishes to use it or parts of it, so it is under the MIT License.

Copyright (c) 2016 Keith Webb

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.