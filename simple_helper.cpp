//
// Created by Keith Webb on 10/15/16.
//

#define DATA_BLOCK_SIZE 1024

#include <fstream>
#include <iostream>

int main(int argc, char* argv[])
{
    if(argc < 3)
    {
        std::cout << "Error, Run with 2 Arguments: <source_file> <destination_file>\n";
        return 1;
    }

    std::string destination_file_path = argv[2];
    std::string source_file_path = argv[1];

    std::ofstream destination_file(destination_file_path, std::ios::trunc | std::ios::binary);
    std::ifstream source_file(source_file_path, std::ios::binary | std::ios::ate);

    std::streampos size = source_file.tellg();
    source_file.seekg (0, std::ios::beg);

    long long int written = 0;

    char data_block[DATA_BLOCK_SIZE];

    while(written < size)
    {
        long long int remaining = size - written;

        if(remaining < DATA_BLOCK_SIZE)
        {
            source_file.read(data_block, remaining);
            destination_file.write(data_block, remaining);
            written += remaining;
        }
        else
        {
            source_file.read(data_block, DATA_BLOCK_SIZE);
            destination_file.write(data_block, DATA_BLOCK_SIZE);
            written += DATA_BLOCK_SIZE;
        }
    }

    destination_file.close();
    source_file.close();

    return 0;
}
